# Change Log

All notable changes to this project will be documented in this file.

## Unreleased
### Added
### Changed
### Deprecated
### Removed
### Fixed
### Security

## 0.1.0 - 2015-06-03
### Added
- Convert YAML into HTML table
- Create a package Chokola
- Options to add custom HTML classes
- CHANGELOG.md and LICENCE file
- `.makaronrc` file

[Unreleased]: https://gitlab.com/kroissan/kroissan/compare/0.1.0...HEAD
